package com.kodsolution.ekleel.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kodsolution.ekleel.R;

/**
 * Created by mac on 3/17/2018 AD.
 */

public class NotificationAdapter   extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    Activity activity;

    public NotificationAdapter(Activity activity){
        this.activity=activity;

    }
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_item, parent, false);

        return new NotificationAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, final int position) {


    }

    @Override
    public int getItemCount() {
        return 5;
    }


    public  class ViewHolder extends RecyclerView.ViewHolder   {


        public ViewHolder(View v) {

            super(v);


        }
    }}