package com.kodsolution.ekleel.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kodsolution.ekleel.R;

/**
 * Created by mac on 3/17/2018 AD.
 */

public class BrandsAdapter   extends RecyclerView.Adapter<BrandsAdapter.ViewHolder> {
    Activity activity;

    public BrandsAdapter(Activity activity){
        this.activity=activity;

    }
    @Override
    public BrandsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.brand_item, parent, false);

        return new BrandsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BrandsAdapter.ViewHolder holder, final int position) {


    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder   {
        public ViewHolder(View v) {
            super(v);
        }
    }}
