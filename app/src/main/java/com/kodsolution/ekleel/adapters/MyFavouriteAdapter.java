package com.kodsolution.ekleel.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kodsolution.ekleel.R;

/**
 * Created by mac on 3/17/2018 AD.
 */

public class MyFavouriteAdapter   extends RecyclerView.Adapter<MyFavouriteAdapter.ViewHolder> {
    Activity activity;

    public MyFavouriteAdapter(Activity activity){
        this.activity=activity;

    }

    @Override
    public MyFavouriteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_favourite_item, parent, false);

        return new MyFavouriteAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyFavouriteAdapter.ViewHolder holder, final int position) {


    }

    @Override
    public int getItemCount() {
        return 5;
    }


    public  class ViewHolder extends RecyclerView.ViewHolder   {


        public ViewHolder(View v) {

            super(v);


        }
    }}
