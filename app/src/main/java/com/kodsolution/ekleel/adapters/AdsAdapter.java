package com.kodsolution.ekleel.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kodsolution.ekleel.R;

import java.util.List;

/**
 * Created by user9 on 3/15/2018.
 */

public class AdsAdapter extends PagerAdapter {
    Activity activity;
    int [] images={R.drawable.test1,R.drawable.test2,R.drawable.test3,R.drawable.test4};
    //constructor


    public AdsAdapter(Activity activity) {
        this.activity=activity;
    }

    @Override
    public int getCount() {
        //// TODO: 10/9/2017  change return number
        return 4;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mLayoutInflater.inflate(R.layout.slider_item, container, false);
        ImageView imageView=(ImageView)v.findViewById(R.id.ads_image);
        imageView.setImageResource(images[position]);



        container.addView(v);
        return v;
    }
    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        container.removeView((FrameLayout) obj);
    }
}
