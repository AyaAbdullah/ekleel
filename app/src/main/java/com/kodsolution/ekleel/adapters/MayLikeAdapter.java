package com.kodsolution.ekleel.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.kodsolution.ekleel.R;

/**
 * Created by user9 on 3/7/2018.
 */

public class MayLikeAdapter  extends RecyclerView.Adapter<MayLikeAdapter.ViewHolder> {
    Activity activity;

    public MayLikeAdapter(Activity activity){
        this.activity=activity;


    }
    @Override
    public MayLikeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.may_like_item, parent, false);

        return new MayLikeAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MayLikeAdapter.ViewHolder holder, final int position) {


    }

    @Override
    public int getItemCount() {
        return 3;
    }


    public  class ViewHolder extends RecyclerView.ViewHolder   {


        public ViewHolder(View v) {

            super(v);


        }
    }}