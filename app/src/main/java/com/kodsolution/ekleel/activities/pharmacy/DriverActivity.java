package com.kodsolution.ekleel.activities.pharmacy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;

public class DriverActivity extends AppCompatActivity {

    private TextView toolbarTxt,downlaod;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        init();
        clicks();
    }

    public void init(){
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        downlaod=(TextView)findViewById(R.id.download_txt);
    }
    public void clicks()
    {

        downlaod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo navigate to link
            }
        });
    }

    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.driver));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));


    }
}
