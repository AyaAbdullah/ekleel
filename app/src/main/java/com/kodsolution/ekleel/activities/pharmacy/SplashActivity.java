package com.kodsolution.ekleel.activities.pharmacy;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.session.MediaController;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.activities.user.EditProfileActivity;
import com.kodsolution.ekleel.activities.user.ForgetPasswordActivity;
import com.kodsolution.ekleel.activities.user.LoginActivity;
import com.kodsolution.ekleel.activities.user.ProfileActivity;
import com.kodsolution.ekleel.activities.user.SignupActivity;
import com.kodsolution.ekleel.tools.SharedPref;

import java.io.IOException;
import java.util.Locale;

import pl.droidsonroids.gif.GifDrawable;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 2600;
    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPref=new SharedPref(SplashActivity.this);
       /* if(sharedPref.getLang()==""|sharedPref.getLang()!=null)
        {
            String language= Locale.getDefault().getDisplayLanguage();
            sharedPref.setLang(language);


        }
        */
        try {
            GifDrawable gifFromResource = new GifDrawable( getResources(), R.drawable.giphy );
            gifFromResource.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                /* Create an Intent that will start home activity */
                Intent i2 = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(i2);
                overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }
}
