package com.kodsolution.ekleel.activities.user;

import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.tools.SharedPref;
import com.kodsolution.ekleel.tools.Utility;

public class ForgetPasswordActivity extends AppCompatActivity {
    private TextView toolbarTxt;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;
    boolean validated =true;
    private TextView sendTxt;
    String mailStr;
    private EditText emailEdt;
    TextInputLayout emailLayout;
    SharedPref sharedPref;
    Typeface tf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        init();
        clicks();
    }
    public void clicks()
    {
        sendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validated =true;
                if(Utility.emailValidation(ForgetPasswordActivity.this,emailEdt,validated))
                {

                }
                else {validated=false;}
                if(validated) {

                    // TODO: 3/3/2018 call api
                    mailStr= emailEdt.getText().toString();
                    Utility.showToast("ok",ForgetPasswordActivity.this);
                }

            }
        });


    }
    public void init ()
    {
        sharedPref=new SharedPref(ForgetPasswordActivity.this);
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        sendTxt =(TextView)findViewById(R.id.send_txt);
        emailLayout=(TextInputLayout)findViewById(R.id.email_layout) ;
        emailEdt= (EditText) findViewById(R.id.email_edt);
        if(sharedPref.getLang().equals("ar")){
            tf = Typeface.createFromAsset(getAssets(), "Cairo-Regular.ttf");
        }
        else {
            tf = Typeface.createFromAsset(getAssets(), "roboto-regular.ttf");

        }
        emailLayout.setTypeface(tf);
        emailEdt.setTypeface(tf);
        emailEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailEdt.setError(null);            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.forget_password));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));


    }
}
