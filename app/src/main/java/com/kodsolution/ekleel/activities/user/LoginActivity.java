package com.kodsolution.ekleel.activities.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.activities.pharmacy.AddPharmacyActivity;
import com.kodsolution.ekleel.activities.pharmacy.DriverActivity;
import com.kodsolution.ekleel.activities.pharmacy.MainActivity;
import com.kodsolution.ekleel.activities.pharmacy.SplashActivity;
import com.kodsolution.ekleel.tools.SharedPref;
import com.kodsolution.ekleel.tools.Utility;

public class LoginActivity extends AppCompatActivity {
   private ImageView facebook,twitter,google;
   private TextView loginTxt,pharmacyTxt,driverTxt,forgetPasswordTxt,signupTxt;
   private EditText emailEdt,passwordEdt;
   private TextInputLayout emailLayout,passwordLayout;
   private SharedPref sharedPref;
   private Typeface tf;
   private boolean validated =false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        clicks();
    }

    public void init()
    {
        sharedPref =new SharedPref(LoginActivity.this);
        if(sharedPref.getLang().equals("ar")){
            tf = Typeface.createFromAsset(getAssets(), "Cairo-Regular.ttf");
        }
        else {
            tf = Typeface.createFromAsset(getAssets(), "roboto-regular.ttf");
        }
        loginTxt=(TextView)findViewById(R.id.login_txt);
        forgetPasswordTxt=(TextView)findViewById(R.id.forget_password);
        signupTxt=(TextView)findViewById(R.id.signup_txt);
        driverTxt=(TextView)findViewById(R.id.driver_txt);
        pharmacyTxt=(TextView)findViewById(R.id.pharmacy_txt);
        emailEdt=(EditText)findViewById(R.id.login_email_edt);
        passwordEdt=(EditText)findViewById(R.id.login_password_edt);
        emailLayout=(TextInputLayout)findViewById(R.id.login_email_layout);
        passwordLayout=(TextInputLayout)findViewById(R.id.login_password_layout);
        emailLayout.setTypeface(tf);
        passwordLayout.setTypeface(tf);
        Spannable spannable = new SpannableString(signupTxt.getText().toString());
        spannable.setSpan(new ForegroundColorSpan(Color.BLACK),signupTxt.getText().toString().length()-15, (36), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        signupTxt.setText(spannable, TextView.BufferType.SPANNABLE);

    }
    public void clicks()
    {
        loginTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validated=true;
                if( Utility.passwordValidation(LoginActivity.this,passwordEdt,validated)){

                }
                else {
                    validated=false;
                }
                if(Utility.emailValidation(LoginActivity.this,emailEdt,validated))
                {}
                else {
                    validated=false;
                }
                if (validated) {
                    //todo call api
                    Intent i2 = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(i2);
                    finish();
                }

            }
        });
        forgetPasswordTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                startActivity(i2);
            }
        });
        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(LoginActivity.this,SignupActivity.class);
                startActivity(i2);
            }
        });
        driverTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(LoginActivity.this,DriverActivity.class);
                startActivity(i2);
            }
        });
        pharmacyTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(LoginActivity.this,AddPharmacyActivity.class);
                startActivity(i2);

            }
        });
    }
}
