package com.kodsolution.ekleel.activities.pharmacy;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.activities.user.SignupActivity;
import com.kodsolution.ekleel.tools.MarshMallowPermission;
import com.kodsolution.ekleel.tools.Utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class AddPharmacyActivity extends AppCompatActivity {
    ImageView uploadImage;
    private Bitmap mBitamp;
    private boolean isHandled;
    private  boolean validated =false;

    MarshMallowPermission marshMallowPermission;
    private static final int MY_PERMISSIONS_REQUEST_OPEN_CAMERA = 123;
    String picturePath="";
    private TextView toolbarTxt,addPharmacyTxt;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;
    private EditText pharmacyNameEdt,emailEdt,mobEdt,passwordEdt,confirmPasswordEdt,aboutPharmacyEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pharmacy);
        init();
        clicks();
    }

    public void init()
    {
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        addPharmacyTxt=(TextView)findViewById(R.id.add_pharmacy_txt) ;
        pharmacyNameEdt=(EditText)findViewById(R.id.pharmacy_name_edt);
        emailEdt=(EditText)findViewById(R.id.email_edt);
        mobEdt=(EditText)findViewById(R.id.mob_edt);
        aboutPharmacyEdt=(EditText)findViewById(R.id.about_pharmacy_edt);
        passwordEdt=(EditText)findViewById(R.id.password_edt);
        confirmPasswordEdt=(EditText)findViewById(R.id.confirm_password_edt);

        uploadImage = (ImageView) findViewById(R.id.pharmacy_image);
        ActivityCompat.requestPermissions(AddPharmacyActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);


    }
    public void clicks(){

        addPharmacyTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validated =true;
                if(Utility.nameValidation(pharmacyNameEdt,AddPharmacyActivity.this,validated)){

                }
                else {
                    validated=false;
                }
                if( Utility.passwordValidation(AddPharmacyActivity.this,passwordEdt,validated)){

                }
                else {
                    validated=false;
                }
                if(Utility.mobileValidation(AddPharmacyActivity.this,mobEdt,validated)){

                }
                else {
                    validated=false;
                }
                if(Utility.emailValidation(AddPharmacyActivity.this,emailEdt,validated))
                {}
                else {
                    validated=false;
                }
                if(Utility.passwordConfirmationValidation(AddPharmacyActivity.this,passwordEdt,confirmPasswordEdt,validated))
                {

                }
                else {
                    validated=false;
                }
                //todo remove validated =true
           //     validated=true;

                if(validated)
                {
                    // todo call api
                    Intent i2 = new Intent(AddPharmacyActivity.this,ConfirmationNumberActivity.class);
                    startActivity(i2);
                }

            }
        });
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Utility.showToast("HIII",SignupActivity.this);
                getImage();
            }
        });

    }

    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.add_pharmacy_str));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));


    }
    public void getImage() {
        final Dialog dialog = new Dialog(AddPharmacyActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.take_photo_dialog);
        ImageView cameraLayout = (ImageView) dialog.findViewById(R.id.camera_imageView);
        ImageView galleryLaout = (ImageView) dialog.findViewById(R.id.gallery_imageView);
        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // for marshmallow only..
                if (ContextCompat.checkSelfPermission(AddPharmacyActivity.this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(AddPharmacyActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_OPEN_CAMERA);

                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 2);
                }
                dialog.dismiss();
            }
        });

        galleryLaout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
                dialog.dismiss();

                */
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }
    public void uploading(Bitmap bitmap) {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        picturePath = Base64.encodeToString(ba, 0);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null && requestCode == 1) {
            Uri uri = data.getData();
            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                Bitmap sent = Bitmap.createScaledBitmap(bm, 500, 500, true);
                mBitamp = sent;
                uploadImage.setImageBitmap(sent);
                uploading(sent);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_OK && data != null && requestCode == 2) {
            Bundle bu = data.getExtras();
            Bitmap bit = (Bitmap) bu.get("data");
            Bitmap sent = Bitmap.createScaledBitmap(bit, 500, 500, true);
            uploadImage.setImageBitmap(sent);
            uploading(sent);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 105:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    isHandled= true;
                } else {
                    Toast.makeText(this, "Please Enable Phone permission to Continue", Toast.LENGTH_SHORT).show();
                    isHandled= false;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
        }
    }
}