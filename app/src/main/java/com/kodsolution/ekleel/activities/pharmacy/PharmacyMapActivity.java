package com.kodsolution.ekleel.activities.pharmacy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.tools.SharedPref;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class PharmacyMapActivity extends  AppCompatActivity implements
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnInfoWindowLongClickListener,
        GoogleMap.OnInfoWindowCloseListener,
        OnMapAndViewReadyListener.OnGlobalLayoutAndMapReadyListener{
    private static final LatLng SYDNEY = new LatLng(-33.87365, 151.20689);
    private GoogleMap mMap;
    private Marker mSydney;
    private Marker mPegman;
    private TextView toolbarTxt;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;

    SharedPref sharedPref;

    //on create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacy_map);
        sharedPref=new SharedPref(PharmacyMapActivity.this);
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        new OnMapAndViewReadyListener(mapFragment, this);
    }
    /** Demonstrates customizing the info window and/or its contents. */
    class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        // These are both viewgroups containing an ImageView with id "badge" and two TextViews with id
        // "title" and "snippet".
        private final View mWindow;
        private final View mContents;
        CustomInfoWindowAdapter() {
            mWindow = getLayoutInflater().inflate(R.layout.pharmacy_map_content_info, null);
            mContents = getLayoutInflater().inflate(R.layout.pharmacy_map_content_info, null);
        }
        @Override
        public View getInfoWindow(Marker marker) {
            render(marker, mWindow);
            return mWindow;
        }
        @Override
        public View getInfoContents(Marker marker) {
            render(marker, mContents);
            return mContents;
        }
        private void render(Marker marker, View view) {


        }
    }
    //on mapReady
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        // Hide the zoom controls as the button panel will cover it.
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        // Add lots of markers to the map.
        addMarkersToMap();

        // Setting an info window adapter allows us to change the both the contents and look of the
        // info window.

        // Set listeners for marker events.  See the bottom of this class for their behavior.
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnInfoWindowCloseListener(this);
        mMap.setOnInfoWindowLongClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        // Override the default content description on the view, for accessibility mode.
        // Ideally this string would be localised.
        mMap.setContentDescription("Map with lots of markers.");

        LatLngBounds bounds = new LatLngBounds.Builder()

                .include(SYDNEY)

                .build();
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 30));
        float zoomLevel = 15.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SYDNEY, zoomLevel));

        mMap.setMinZoomPreference(0.0f);
        mMap.setMaxZoomPreference(30.0f);
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

    }

    //add markers to map resultActivity
    private void addMarkersToMap() {
        // Uses a colored icon.

        mPegman = mMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cart))
                .rotation(30)
                .title("")
                .draggable(true));

        // Uses a custom icon with the info window popping out of the center of the icon.
        mSydney = mMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .title("150 kwd")
                .rotation(-10)
                .snippet("Population: 4,627,300")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cart))
                .infoWindowAnchor(0.5f, 0.5f));
    }


    // action on marker to show window info with animation
    @Override
    public boolean onMarkerClick(final Marker marker) {
        if(marker.getTitle().equals("")) {
            marker.hideInfoWindow();
            return true;
        }
        else {
            return false;
        }

    }

    //action on infoWindow >>go to HotelDetailsActivity

    @Override
    public void onInfoWindowClick(Marker marker) {
       // write action
    }

    //action on InfoWindowClose
    @Override
    public void onInfoWindowClose(Marker marker) {

    }
    //action on infoWindow long click>>go to HotelDetailsActivity

    @Override
    public void onInfoWindowLongClick(Marker marker) {

    }

    //action on start drag
    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    //action after end drag >> go to steetview resultActivity

    @Override
    public void onMarkerDragEnd(Marker marker) {
      /*  Intent intent=new Intent(MapHotelActivity.this,StreetViewActivity.class);
        intent.putExtra("lat",marker.getPosition().latitude);
        intent.putExtra("lng",marker.getPosition().longitude);
        startActivity(intent);
        */

    }
    // action on marker drag
    @Override
    public void onMarkerDrag(Marker marker) {

    }

    /**back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }



    public class MarkerCallback implements Callback {
        Marker marker=null;

        MarkerCallback(Marker marker) {
            this.marker=marker;
        }

        @Override
        public void onError() {
            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
        }
    }


}
