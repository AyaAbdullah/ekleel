package com.kodsolution.ekleel.activities.user;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.activities.pharmacy.ConfirmationNumberActivity;
import com.kodsolution.ekleel.activities.pharmacy.MainActivity;
import com.kodsolution.ekleel.tools.SharedPref;
import com.kodsolution.ekleel.tools.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SignupActivity extends AppCompatActivity {
    boolean validated =false;
    private TextView toolbarTxt,send;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;
    private Typeface tf;
    private TextInputLayout passwordLayout,confirmedPasswordLayout,emailLayout,mobLayout,nameLayout;
    private SharedPref sharedPref;
    private EditText passwordEdt, emailEdt, mobEdt, nameEdt,confirmPasswordEdt;
    private Spinner genderSpiner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        init();
        clicks();
    }

    public  void init ()
    {
        sharedPref = new SharedPref(SignupActivity.this);

        //toolbar
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();

        send=(TextView)findViewById(R.id.send_txt) ;
        emailLayout=(TextInputLayout) findViewById(R.id.signup_email_layout);
        passwordLayout=(TextInputLayout)findViewById(R.id.signup_password_layout);
        confirmedPasswordLayout=(TextInputLayout)findViewById(R.id.signup_confirm_password_layout) ;
        nameLayout=(TextInputLayout)findViewById(R.id.signup_name_layout);
        mobLayout=(TextInputLayout)findViewById(R.id.signup_mob_layout);
        confirmPasswordEdt=(EditText)findViewById(R.id.confirm_password_edt);
        nameEdt = (EditText) findViewById(R.id.signup_name_edt);
        mobEdt = (EditText) findViewById(R.id.signup_mob_edt);
        passwordEdt = (EditText) findViewById(R.id.password_edt);
        emailEdt = (EditText) findViewById(R.id.signup_email_edt);

        if(sharedPref.getLang().equals("ar")){
            tf = Typeface.createFromAsset(getAssets(), "Cairo-Regular.ttf");
        }
        else {
            tf = Typeface.createFromAsset(getAssets(), "roboto-regular.ttf");

        }
        mobLayout.setTypeface(tf);
        nameLayout.setTypeface(tf);
        emailLayout.setTypeface(tf);
        passwordLayout.setTypeface(tf);
        confirmedPasswordLayout.setTypeface(tf);
        mobEdt.setTypeface(tf);
        nameEdt.setTypeface(tf);
        emailEdt.setTypeface(tf);
        passwordEdt.setTypeface(tf);
        confirmPasswordEdt.setTypeface(tf);
        genderSpiner = (Spinner) findViewById(R.id.title_sp);
        List<String> titleList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.gender)));
        SpinersAdapter titleAdapter = new SpinersAdapter(titleList);
        genderSpiner.setAdapter(titleAdapter);
        genderSpiner.setSelection(0);


    }
    public void clicks(){
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validated =true;
                if(Utility.nameValidation(nameEdt,SignupActivity.this,validated)){

                }
                else {
                    validated=false;
                }
                if( Utility.passwordValidation(SignupActivity.this,passwordEdt,validated)){

                }
                else {
                    validated=false;
                }
                if(Utility.mobileValidation(SignupActivity.this,mobEdt,validated)){

                }
                else {
                    validated=false;
                }
                if(Utility.emailValidation(SignupActivity.this,emailEdt,validated))
                {}
                else {
                    validated=false;
                }
                if(Utility.passwordConfirmationValidation(SignupActivity.this,passwordEdt,confirmPasswordEdt,validated))
                {

                }
                else {
                    validated=false;
                }
        //todo remove validated =true
                validated=true;

                if(validated)
                {
                    // todo call api
                    Intent i2 = new Intent(SignupActivity.this,ConfirmationNumberActivity.class);
                    startActivity(i2);
                }

            }
        });


    }

    /**
     *  back from mobile
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.sign_up));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));
    }

    private class SpinersAdapter extends BaseAdapter {
        List<String> groupList;

        public SpinersAdapter(List<String> groupList) {
            this.groupList = groupList;
        }

        @Override
        public int getCount() {
            return groupList.size();
        }

        @Override
        public Object getItem(int position) {
            return groupList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.new_spinner_item, parent, false);
            TextView textView = (TextView) view.findViewById(R.id.spin_txt);
            textView.setText(groupList.get(position));

            return view;
        }
    }

}
