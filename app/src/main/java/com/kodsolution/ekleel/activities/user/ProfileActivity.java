package com.kodsolution.ekleel.activities.user;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.tools.FixAppBarLayoutBehavior;

public class ProfileActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private FloatingActionButton edit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
        clicks();
    }
    public void init()
    {
        edit=(FloatingActionButton)findViewById(R.id.edit) ;
        CollapsingToolbarLayout c = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        c.setTitleEnabled(false);
        AppBarLayout abl = findViewById(R.id.appBar);
        ((CoordinatorLayout.LayoutParams) abl.getLayoutParams()).setBehavior(new FixAppBarLayoutBehavior());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setTitle("Profile");
    }
    public void clicks()
    {

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(ProfileActivity.this,EditProfileActivity.class);
                startActivity(i2);
            }
        });
    }
}
