package com.kodsolution.ekleel.activities.user;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.adapters.MyFavouriteAdapter;
import com.kodsolution.ekleel.adapters.NotificationAdapter;

public class MyFavouriteActivity extends AppCompatActivity {

    private TextView toolbarTxt;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;
    private RecyclerView myFavouriteRec;
    private MyFavouriteAdapter myFavouriteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favourite);
        init();
        clicks();
    }
    public void init()
    {
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        myFavouriteRec=(RecyclerView)findViewById(R.id.my_fav_rec);
        myFavouriteAdapter = new MyFavouriteAdapter(MyFavouriteActivity.this);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(MyFavouriteActivity.this,3);
        myFavouriteRec.setLayoutManager(gridLayoutManager);
        myFavouriteRec.setAdapter(myFavouriteAdapter);


    }
    public void clicks()
    {

    }
    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.my_favourite));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));


    }
}
