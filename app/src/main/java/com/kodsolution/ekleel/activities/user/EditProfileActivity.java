package com.kodsolution.ekleel.activities.user;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.fragments.user.EditAddressFragment;
import com.kodsolution.ekleel.fragments.user.EditPasswordFragment;
import com.kodsolution.ekleel.fragments.user.EditProfileFragment;
import com.kodsolution.ekleel.tools.CustomTabLayout;
import com.kodsolution.ekleel.tools.SharedPref;

public class EditProfileActivity extends AppCompatActivity {

    private CustomTabLayout tabLayout;
    private ViewPager viewPager;
    private SharedPref sharedPref;
    private TextView toolbarTxt;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
        setupTabIcons();
    }

    /*initialization of components
      @Return nothing
      * */
    public void init() {
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (CustomTabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        sharedPref = new SharedPref(getApplicationContext());

    }
    /* pager class to return fragments depend on tab position
    * pager constructor take number of tabs
    * */
    public class Pager extends FragmentStatePagerAdapter {
        private static final String TAG = "Pager";
        //integer to count number of tabs
        int tabCount;
        //Constructor to the class
        public Pager(FragmentManager fragmentManager, int tabCount) {
            super(fragmentManager);
            this.tabCount = tabCount;

        }
        //Overriding method getItem
        @Override
        public Fragment getItem(int position) {
            //Returning the current tabs
            switch (position) {
                case 0:
                    return new EditProfileFragment();
                case 1:
                    return new EditAddressFragment();
                case 2:
                    return new EditPasswordFragment();

                default:
                    return new EditProfileFragment();
            }
        }
        //Overriden method getCount to get the number of tabs
        @Override
        public int getCount() {
            return tabCount;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && viewPager.getCurrentItem() != 0) {
            viewPager.setCurrentItem(0, true);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
    // setub view pager ,we can add here if we want to make some thing after page selected
    private void setupViewPager(ViewPager viewPager) {
        Pager pager = new Pager(getSupportFragmentManager(), 3);

        viewPager.setAdapter(pager);
        viewPager.setCurrentItem(0);
        float sizeInDip = 16f;
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, sizeInDip, getResources().getDisplayMetrics());
        viewPager.setPageMargin(margin);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public int prePosition;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }
    // setup tab text and icons
    private void setupTabIcons() {
        tabLayout.getTabAt(0).setText("Profile");
        tabLayout.getTabAt(1).setText("Address");
        tabLayout.getTabAt(2).setText("Password");

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv=(TextView) LayoutInflater.from(EditProfileActivity.this).inflate(R.layout.custom_tab,null);

            if(sharedPref.getLang().equals("ar"))
            {
                tv.setTypeface(Typeface.createFromAsset(EditProfileActivity.this.getAssets(), "Cairo-Bold.ttf"));

            }else {
                tv.setTypeface(Typeface.createFromAsset(EditProfileActivity.this.getAssets(), "roboto-bold.ttf"));

            }
            tabLayout.getTabAt(i).setCustomView(tv);

        }

    }
    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.ediProfile_str));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));


    }


}