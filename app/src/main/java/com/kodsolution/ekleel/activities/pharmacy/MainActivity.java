package com.kodsolution.ekleel.activities.pharmacy;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.activities.user.LoginActivity;
import com.kodsolution.ekleel.activities.user.MyFavouriteActivity;
import com.kodsolution.ekleel.activities.user.MyOrdersActivity;
import com.kodsolution.ekleel.activities.user.NotificationActivity;
import com.kodsolution.ekleel.activities.user.ProfileActivity;
import com.kodsolution.ekleel.activities.user.SignupActivity;
import com.kodsolution.ekleel.fonts.CustomTypefaceSpan;
import com.kodsolution.ekleel.fragments.pharmacy.MainNavigationFragment;
import com.kodsolution.ekleel.tools.IntentClass;
import com.kodsolution.ekleel.tools.SharedPref;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    //tags for replacing frag in navigation drawer
    public final static String TAG_HOME = "home";
    private Typeface tf;

    MenuItem mi;
    public static Activity activity;
    private NavigationView navigationView;
    private Menu nav_Menu;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_HOME;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private SharedPref sharedPref;
    private TextView userNameTxt, title;
    private ImageView notificationImg,cartImg,searchImg;
    TelephonyManager tm;
    Intent intent;
    // on create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //check language
        setupLanguage();
        if(sharedPref.getLang().equals("ar")){
            tf = Typeface.createFromAsset(getAssets(), "Cairo-Regular.ttf");
        }
        else {
            tf = Typeface.createFromAsset(getAssets(), "roboto-regular.ttf");

        }
        //set content of activity
        setContentView(R.layout.activity_main);
        //initialization
        init(savedInstanceState);
        clicks();

    }

    /**
     *initialization
     * @return nothing
     */
    public void init(Bundle savedInstanceState) {
        //initilization of toolbar
        title = (TextView) findViewById(R.id.title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionbar = getSupportActionBar();


        notificationImg = (ImageView) toolbar.findViewById(R.id.notification_img);
        cartImg = (ImageView) toolbar.findViewById(R.id.cart_img);
        searchImg = (ImageView) toolbar.findViewById(R.id.search_img);

        //initialization of navigartion drawer
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);



        nav_Menu = navigationView.getMenu();
        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
       userNameTxt=(TextView)headerView.findViewById(R.id.user_name);
       userNameTxt.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i2 = new Intent(MainActivity.this,ProfileActivity.class);
               startActivity(i2);
           }
       });

        setUpNavigationView();
        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();

        }

        for (int i=0;i<nav_Menu.size();i++) {
            mi = nav_Menu.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    }
    public void clicks()
    {
        notificationImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(MainActivity.this,NotificationActivity.class);
                startActivity(i2);
            }
        });
        cartImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(MainActivity.this,CartActivity.class);
                startActivity(i2);
            }
        });
        searchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(MainActivity.this,SearchActivity.class);
                startActivity(i2);
            }
        });
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font ;
        if(sharedPref.getLang().equals("ar"))
        {
            font = Typeface.createFromAsset(getAssets(), "Cairo-Regular.ttf");
        }
        else
        {
            font = Typeface.createFromAsset(getAssets(), "roboto-regular.ttf");
        }
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
    /**
     *get fragment
     * @return nothing
     */

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                return  new MainNavigationFragment();
            case 1:
                return new MainNavigationFragment();

            default:
                return new MainNavigationFragment();
        }
    }

    /**
     *loading home fragment
     * @return nothing
     */
    private void loadHomeFragment() {
        setToolbarTitle();

        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            return;
        }

        Fragment fragment = getHomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
        drawer.closeDrawers();
        invalidateOptionsMenu();
    }

    /**
     *toolbar title
     * @return nothing
     */
    private void setToolbarTitle() {
        //getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }
    /**
     *setup navigation drawer
     * switch between navigation index to replace home fragment
     * @return nothing
     */
    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        getSupportActionBar().setDisplayShowTitleEnabled(false);
                        break;
                    case R.id.nav_language:
                        changeLangDialog();
                        break;

                    case R.id.nav_share_app:
                        ShareIntent();
                        break;
                    case R.id.nav_pharmacy_map:
                        intent = new Intent(MainActivity.this,PharmacyMapActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.nav_aboutus:
                       intent = new Intent(MainActivity.this,AboutUsActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.nav_contactus:
                        intent = new Intent(MainActivity.this,ContactUsActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.nav_favourite:
                        intent = new Intent(MainActivity.this,MyFavouriteActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.nav_orders:
                     intent = new Intent(MainActivity.this,MyOrdersActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_terms:
                     intent = new Intent(MainActivity.this,TermsAndConditionsActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_add_pharmacy:
                      intent = new Intent(MainActivity.this,AddPharmacyActivity.class);
                        startActivity(intent);
                        break;

                    default:
                        navItemIndex = 0;
                }

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }


                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });

    final     ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.openDrawer, R.string.closeDrawer)

    {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {


                for (int i=0;i<nav_Menu.size();i++) {
                    mi = nav_Menu.getItem(i);

                    //for aapplying a font to subMenu ...
                    SubMenu subMenu = mi.getSubMenu();
                    if (subMenu!=null && subMenu.size() >0 ) {
                        for (int j=0; j <subMenu.size();j++) {
                            MenuItem subMenuItem = subMenu.getItem(j);
                            applyFontToMenuItem(subMenuItem);
                        }
                    }
                    //the method we have create in activity
                    applyFontToMenuItem(mi);
                }
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        toolbar.setNavigationIcon(R.drawable.ic_menu);

        drawer.setDrawerListener(actionBarDrawerToggle);

      //  actionBarDrawerToggle.syncState();
    }

    /**
     *setup back from mobile
     * @Override onBackPressed
     */
    @Override
    public void onBackPressed() {


        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        if (shouldLoadHomeFragOnBackPress) {

            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }
    /**
     * check language
     * geting flag from shared pref to know language
     * to know language get varialble getLang from shared pref if ar set application language arabic if en set it english
     * @return nothing
     */
    public void setupLanguage(){

        // geting flag from shared pref to know language
        sharedPref = new SharedPref(getApplicationContext());
        //to know language get varialble getLang from shared pref if ar set application language arbic if en set it english
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        activity = this;

        if (sharedPref.getLang().equals("ar")) {
            conf.setLocale(new Locale("ar"));
        } else {
            conf.setLocale(new Locale("en"));
        }

        res.updateConfiguration(conf, dm);

    }

    // change language
    public void changeLangDialog(){
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Dialog dialog = new Dialog(MainActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View dialogView = inflater.inflate(R.layout.language_dialog, null);
        final RadioButton en_option = (RadioButton) dialogView.findViewById(R.id.en_option);
        final RadioButton ar_option = (RadioButton) dialogView.findViewById(R.id.ar_option);
        ar_option.setTypeface(tf);
        en_option.setTypeface(tf);

        if(sharedPref.getLang().equals("ar"))
        {
            ar_option.setChecked(true);

        }
        else {
            en_option.setChecked(true);
        }

        en_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPref.setLang("en");
                IntentClass.goToActivityAndClear(MainActivity.this,MainActivity.class,null);
            }
        });

        ar_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPref.setLang("ar");
                IntentClass.goToActivityAndClear(MainActivity.this,MainActivity.class,null);
            }
        });

        dialog.setContentView(dialogView);
        dialog.show();
    }

    // to share link of app
    public void ShareIntent()
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out Ekleel at: https://play.google.com/store/apps/details?id=com.google.android.apps.plus");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

}
