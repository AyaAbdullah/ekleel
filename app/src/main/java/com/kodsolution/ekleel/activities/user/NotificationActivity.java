package com.kodsolution.ekleel.activities.user;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.adapters.BrandsAdapter;
import com.kodsolution.ekleel.adapters.NotificationAdapter;
import com.kodsolution.ekleel.adapters.ProudctsAdapter;

public class NotificationActivity extends AppCompatActivity {
    private TextView toolbarTxt;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;
    private RecyclerView notificationRec;
    private NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        init();
    }

    public  void init()
    {
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        notificationRec=(RecyclerView)findViewById(R.id.products_rec);
        notificationAdapter=new NotificationAdapter(NotificationActivity.this);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(NotificationActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        notificationRec.setLayoutManager(linearLayoutManager);
        notificationRec.setAdapter(notificationAdapter);

    }
    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.notification));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));


    }

}
