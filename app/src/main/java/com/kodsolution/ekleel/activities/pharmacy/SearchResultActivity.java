package com.kodsolution.ekleel.activities.pharmacy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;

public class SearchResultActivity extends AppCompatActivity {

    private TextView toolbarTxt;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        init();
        clicks();
    }

    public void init ()
    {

        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();

    }
    public void clicks()
    {

    }

    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.notification));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));


    }
}
