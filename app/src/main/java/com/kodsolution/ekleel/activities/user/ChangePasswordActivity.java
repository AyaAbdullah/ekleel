package com.kodsolution.ekleel.activities.user;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.tools.SharedPref;
import com.kodsolution.ekleel.tools.Utility;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText passwordEdt,confirm_password_edt,oldPasswordEdt;
    String passStr,oldPassStr,confirmPass;
    private TextView toolbarTxt,savePassword;
    private Toolbar toolbar;
    private LinearLayout toolbarLinearLayout;
    boolean validated =true;
    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        init();
        clicks();
    }

    public  void  init(){

        sharedPref=new SharedPref(ChangePasswordActivity.this);
        toolbarLinearLayout=(LinearLayout)findViewById(R.id.layout) ;
        toolbar=(Toolbar)toolbarLinearLayout.findViewById(R.id.toolbar);
        toolbarTxt=(TextView)toolbarLinearLayout.findViewById(R.id.toolbar_txt);
        toolbarSetting();
        savePassword=(TextView)findViewById(R.id.save_password_txt) ;
        confirm_password_edt=(EditText) findViewById(R.id.confirm_password_edt);
        passwordEdt=(EditText)findViewById(R.id.password_edt) ;
        oldPasswordEdt=(EditText)findViewById(R.id.old_password_edt);

        oldPasswordEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Utility.passwordValidation(ChangePasswordActivity.this,oldPasswordEdt,validated);

            }
        });
        passwordEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Utility.passwordValidation(ChangePasswordActivity.this,passwordEdt,validated);

            }
        });
        confirm_password_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Utility.passwordConfirmationValidation(ChangePasswordActivity.this,passwordEdt,confirm_password_edt,validated);

            }
        });

    }
    public void clicks()
    {

        savePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validated =true;

                if(  Utility.passwordConfirmationValidation(ChangePasswordActivity.this,passwordEdt,confirm_password_edt,validated)){

                }
                else {
                    validated=false;
                }
                if( Utility.passwordValidation(ChangePasswordActivity.this,passwordEdt,validated)){

                }
                else {
                    validated=false;
                }
                if( Utility.passwordValidation(ChangePasswordActivity.this,oldPasswordEdt,validated)){

                }
                else {
                    validated=false;
                }


                if(validated) {

                    //// TODO:  api
                    passStr=passwordEdt.getText().toString();
                    oldPassStr=oldPasswordEdt.getText().toString();
                    confirmPass= confirm_password_edt.getText().toString();


                }
            }
        });

    }

    /**
     *  back
     * @return true
     * @Override navigation
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    /**
     * setup toolbar to support title and back
     * @return nothing
     */
    public void toolbarSetting(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarTxt.setText(getString(R.string.changePassword_str));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));
    }
}
