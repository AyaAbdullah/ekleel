package com.kodsolution.ekleel.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import com.kodsolution.ekleel.tools.SharedPref;


@SuppressLint("AppCompatCustomView")
public class CustomTextViewBold extends TextView {
    Typeface face;
    SharedPref sharedPref;
    public CustomTextViewBold(Context context) {
        super(context);
        sharedPref=new SharedPref(context);
        if(sharedPref.getLang().equals("ar"))
        {
            face= Typeface.createFromAsset(context.getAssets(), "Cairo-Bold.ttf");


        }else {

        }    face= Typeface.createFromAsset(context.getAssets(), "roboto-bold.ttf");

        this.setTypeface(face);

    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharedPref=new SharedPref(context);
        if(sharedPref.getLang().equals("ar"))
        {
            face= Typeface.createFromAsset(context.getAssets(), "Cairo-Bold.ttf");


        }else {
            face= Typeface.createFromAsset(context.getAssets(), "roboto-bold.ttf");

        }        this.setTypeface(face);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        sharedPref=new SharedPref(context);
        if(sharedPref.getLang().equals("ar"))
        {
            face= Typeface.createFromAsset(context.getAssets(), "Cairo-Bold.ttf");


        }else {
            face= Typeface.createFromAsset(context.getAssets(), "roboto-bold.ttf");

        }
        this.setTypeface(face);
    }



}
