package com.kodsolution.ekleel.fonts;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.kodsolution.ekleel.tools.SharedPref;


@SuppressLint("AppCompatCustomView")
public class CustomTextView extends TextView {
    Typeface face;
    SharedPref sharedPref;
    public CustomTextView(Context context) {
        super(context);
        sharedPref=new SharedPref(context);
        if(sharedPref.getLang().equals("ar")) {
            face = Typeface.createFromAsset(context.getAssets(), "Cairo-Regular.ttf");
        }
        else {
            face= Typeface.createFromAsset(context.getAssets(), "roboto-regular.ttf");

        }

        this.setTypeface(face);

    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharedPref=new SharedPref(context);
        if(sharedPref.getLang().equals("ar")) {
            face = Typeface.createFromAsset(context.getAssets(), "Cairo-Regular.ttf");
        }
        else {
            face= Typeface.createFromAsset(context.getAssets(), "roboto-regular.ttf");

        }

        this.setTypeface(face);

    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        sharedPref=new SharedPref(context);
        if(sharedPref.getLang().equals("ar")) {
            face = Typeface.createFromAsset(context.getAssets(), "Cairo-Regular.ttf");
        }
        else {
            face= Typeface.createFromAsset(context.getAssets(), "roboto-regular.ttf");

        }


        this.setTypeface(face);

    }

}

