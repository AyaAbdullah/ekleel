package com.kodsolution.ekleel.fonts;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


@SuppressLint("AppCompatCustomView")
public class CustomLightTextView extends TextView {

    public CustomLightTextView(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "roboto-light.ttf");
        this.setTypeface(face);

    }

    public CustomLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "roboto-light.ttf");
        this.setTypeface(face);
    }

    public CustomLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "roboto-light.ttf");
        this.setTypeface(face);
    }

}

