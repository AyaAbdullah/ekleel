package com.kodsolution.ekleel.tools;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by user9 on 3/7/2018.
 */

public class CustomTabLayout   extends TabLayout {
    SharedPref   sharedPref;
    Typeface typeface;

    public CustomTabLayout(Context context) {
        super(context);
        sharedPref=new SharedPref(context);

    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharedPref=new SharedPref(context);

    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        sharedPref=new SharedPref(context);

    }

    @Override
    public void setTabsFromPagerAdapter(@NonNull PagerAdapter adapter) {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Cairo-Bold.ttf");
    /*    if(sharedPref.getLang().equals("ar"))
        {
            typeface= Typeface.createFromAsset(getContext().getAssets(), "Cairo-Bold.ttf");


        }else {
            typeface= Typeface.createFromAsset(getContext().getAssets(), "roboto-bold.ttf");

        }
        */
        this.removeAllTabs();

        ViewGroup slidingTabStrip = (ViewGroup) getChildAt(0);

        for (int i = 0, count = adapter.getCount(); i < count; i++) {
            Tab tab = this.newTab();
            this.addTab(tab.setText(adapter.getPageTitle(i)));
            AppCompatTextView view = (AppCompatTextView) ((ViewGroup)slidingTabStrip.getChildAt(i)).getChildAt(1);
            view.setTypeface(typeface, Typeface.NORMAL);
        }
    }
}