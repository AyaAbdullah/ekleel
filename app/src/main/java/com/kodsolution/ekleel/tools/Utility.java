package com.kodsolution.ekleel.tools;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kodsolution.ekleel.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user9 on 2/28/2018.
 */

public class Utility {

    public static void showToast(String message, Activity activity) {
        if ( activity!= null)
            Toast.makeText(activity,message,Toast.LENGTH_LONG).show();
    }
    public static void showToast(String message, Context context) {
        if ( context!= null)
            Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
    public static boolean emailValidation(Activity activity, EditText emailEdt, boolean validated ){
        validated=true;
        if (!Utility.isEmailValid(emailEdt.getText().toString().trim())) {
            emailEdt.setError(activity.getString(R.string.valid_email_str));
            validated = false;

        } else {
        }
        return validated;
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    public static boolean passwordConfirmationValidation(Activity activity, EditText passwordEdt , EditText confirmPasswordEdt, boolean validated){
        validated=true;
        if (!confirmPasswordEdt.getText().toString().equals(passwordEdt.getText().toString())) {
            confirmPasswordEdt.setError(activity.getString(R.string.confirmed_error_password_str));


            validated = false;

        } else {
        }
        return validated;

    }
    public static boolean nameValidation(EditText nameEdt,Activity activity,boolean validated ) {
        validated=true;
        if (nameEdt.getText().length() < 2) {
            nameEdt.setError(activity.getString(R.string.name_error_str));

            validated = false;

        } else {
        }

        return validated;
    }
    public static boolean mobileValidation(Activity activity,EditText mobEdt  ,boolean validated) {
        validated=true;
        if (mobEdt.getText().length() < 11) {
            mobEdt.setError(activity.getString(R.string.mobile_error_str));

            validated = false;

        } else {
        }
        return validated;
    }
    public static boolean passwordValidation(Activity activity,EditText passwordEdt  ,boolean validated){

        validated=true;
        if (isValidPassword(passwordEdt.getText().toString())) {

            passwordEdt.setError(activity.getString(R.string.new_password_error_str));
            validated = false;

        } else {
        }
        return validated;

    }
    public static boolean isValidPassword(String passwordEd) {
        String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[a-zA-Z\\dd$@$!%*?&#]{6,}$";
        //"^(?=.*[A-Z])(?=.*[@_.]).*$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(passwordEd);
        if (!passwordEd.matches(".*\\d.*") || !matcher.matches()) {
            return true;
        }
        return false;
    }


}
