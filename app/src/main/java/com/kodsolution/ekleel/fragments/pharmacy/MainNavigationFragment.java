package com.kodsolution.ekleel.fragments.pharmacy;

import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.adapters.AdsAdapter;
import com.kodsolution.ekleel.adapters.BrandsAdapter;
import com.kodsolution.ekleel.adapters.ProudctsAdapter;


public class MainNavigationFragment extends android.support.v4.app.Fragment {

    private View view;
    private RecyclerView productsRec,brandsRec;
    private BrandsAdapter brandsAdapter;
    private ProudctsAdapter proudctsAdapter;
    private ViewPager adsPager;
    private ImageView img1, img2, img3, img4, img5,nextImg,backImg;
    private TransitionDrawable[] drawables = new TransitionDrawable[5];
    private AdsAdapter adsAdapter;


    // on create view
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view = inflater.inflate(R.layout.main_frag, container, false);
        init();
        clicks();
         return  view;

    }
    //initialization of components
    public void init (){
        //init
        nextImg=(ImageView)view.findViewById(R.id.next_img) ;
        backImg=(ImageView)view.findViewById(R.id.back_img) ;

        productsRec=(RecyclerView)view.findViewById(R.id.products_rec);
        brandsRec=(RecyclerView)view.findViewById(R.id.brands_catg_rec);
        brandsAdapter= new BrandsAdapter(getActivity());
        proudctsAdapter=new ProudctsAdapter(getActivity());
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        brandsRec.setLayoutManager(linearLayoutManager);
        brandsRec.setAdapter(brandsAdapter);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(),3);
        productsRec.setLayoutManager(gridLayoutManager);
        productsRec.setAdapter(proudctsAdapter);
        adsPager = (ViewPager) view.findViewById(R.id.adsPager);
        adsAdapter = new AdsAdapter(getActivity());
        adsPager.setAdapter(adsAdapter);
        img1 = (ImageView) view.findViewById(R.id.button);
        img2 = (ImageView) view.findViewById(R.id.button2);
        img3 = (ImageView) view.findViewById(R.id.button3);
        img4 = (ImageView) view.findViewById(R.id.button4);
        img5 = (ImageView) view.findViewById(R.id.button5);
        repeatedSlideShow();
        indicators();


        drawables[0] = (TransitionDrawable) img1.getDrawable();
        drawables[1] = (TransitionDrawable) img2.getDrawable();
        drawables[2] = (TransitionDrawable) img3.getDrawable();
        drawables[3] = (TransitionDrawable) img4.getDrawable();
        drawables[4] = (TransitionDrawable) img5.getDrawable();

        drawables[0].startTransition(0);

        adsPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                handleIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    public void repeatedSlideShow() {

        final Handler h = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                animateandSlideShow();
                h.postDelayed(this, 5 * 1000);
            }
        };
        h.postDelayed(runnable, 5 * 1000);
    }

    public void animateandSlideShow() {
        int count = adsPager.getCurrentItem();
        if (count == 5 - 1) {
            count = 0;
            adsPager.setCurrentItem(count, true);
        } else {
            count++;
            adsPager.setCurrentItem(count, true);
        }
    }

    public void handleIndicator(int position) {
        if (drawables != null) {
            for (int i = 0; i < drawables.length; i++) {
                if (i == position) {
                    drawables[i].startTransition(0);
                } else {
                    drawables[i].resetTransition();
                }
            }
        }
    }

    public void indicators(){
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.VISIBLE);
        img3.setVisibility(View.VISIBLE);
        img4.setVisibility(View.VISIBLE);

    }
    //actions on components
    public void clicks() {
        nextImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(adsPager.getCurrentItem()<=4)
                {  adsPager.setCurrentItem(adsPager.getCurrentItem()+1);}

            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(adsPager.getCurrentItem()!=0)
                {  adsPager.setCurrentItem(adsPager.getCurrentItem()-1);}

            }
        });


    }

    }

