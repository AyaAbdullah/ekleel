package com.kodsolution.ekleel.fragments.user;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kodsolution.ekleel.R;
import com.kodsolution.ekleel.activities.user.ChangePasswordActivity;
import com.kodsolution.ekleel.tools.SharedPref;
import com.kodsolution.ekleel.tools.Utility;

/**
 * Created by user9 on 3/19/2018.
 */

public class EditPasswordFragment  extends android.support.v4.app.Fragment {
    private View view;
    private EditText passwordEdt,confirm_password_edt,oldPasswordEdt;
    private boolean validated =true;
    private SharedPref sharedPref;
    private TextView savePassword;
    private String passStr,oldPassStr,confirmPass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.edit_password_frag, container, false);
        init();
        clicks();
        return  view;
    }
    //initialization of components
    public  void  init(){

        sharedPref=new SharedPref(getActivity());

        savePassword=(TextView)view.findViewById(R.id.save_password_txt) ;
        confirm_password_edt=(EditText) view.findViewById(R.id.confirm_password_edt);
        passwordEdt=(EditText)view.findViewById(R.id.password_edt) ;
        oldPasswordEdt=(EditText)view.findViewById(R.id.old_password_edt);

        oldPasswordEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Utility.passwordValidation(getActivity(),oldPasswordEdt,validated);

            }
        });
        passwordEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Utility.passwordValidation(getActivity(),passwordEdt,validated);

            }
        });
        confirm_password_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Utility.passwordConfirmationValidation(getActivity(),passwordEdt,confirm_password_edt,validated);

            }
        });

    }
    // action in components
    public void clicks()
    {

        savePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validated =true;

                if(  Utility.passwordConfirmationValidation(getActivity(),passwordEdt,confirm_password_edt,validated)){

                }
                else {
                    validated=false;
                }
                if( Utility.passwordValidation(getActivity(),passwordEdt,validated)){

                }
                else {
                    validated=false;
                }
                if( Utility.passwordValidation(getActivity(),oldPasswordEdt,validated)){

                }
                else {
                    validated=false;
                }


                if(validated) {

                    //// TODO:  api
                    passStr=passwordEdt.getText().toString();
                    oldPassStr=oldPasswordEdt.getText().toString();
                    confirmPass= confirm_password_edt.getText().toString();


                }
            }
        });

    }

}
